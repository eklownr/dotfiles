#!/bin/bash

# apt update and upgrade
sudo apt update && sudo apt upgrade -y

# Add apt program to install
apt_list=("vim" "nvim" "kitty" "brave-browser" "zoxide" "fzf" "btop" "wget" "curl")

for program in "${apt_list[@]}"; do
	if command -v $program &>/dev/null; then
		echo "$program is already installed"
	else
		sudo apt install $program
	fi
done

# Check if starship is installed, Not an apt
if command -v starship &>/dev/null; then
	echo "starship is already installed"
else
	sudo curl -sS https://starship.rs/install.sh | sh
fi
