# import re
# import socket
# from libqtile.widget import Spacer
# from typing import List  # noqa: F401
# from libqtile.config import Group, ScratchPad, DropDown, Key
import os
import subprocess
from libqtile import bar, widget, hook, qtile
from libqtile.config import Drag, Group, Key, Match, Screen  # , ScratchPad, DropDown
from libqtile.lazy import lazy
from libqtile.layout.xmonad import MonadTall, MonadWide
from libqtile.layout.floating import Floating
from libqtile.layout.max import Max
from libqtile.layout.matrix import Matrix
from libqtile.layout.bsp import Bsp
from libqtile.layout.ratiotile import RatioTile

# import arcobattery


# mod4 or mod = super key
mod = "mod4"
mod1 = "alt"
mod2 = "control"
home = os.path.expanduser("~")


@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)


@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


# @lazy.function
# def set_bg_image(qtile):
#     qtile.lazy.screen.set_wallpaper(home + "/bg/road.jpg", mode="stretch")
### TEST funkar ej


# open kitty
@lazy.function
def open_kitty(qtile):
    qtile.cmd_spawn("kitty")


nightmode_counter = 1


# nightmode light
@lazy.function
def nightmode(qtile):
    global nightmode_counter
    if nightmode_counter == 1:
        qtile.cmd_spawn("alacritty -e nightmode1.sh")
    elif nightmode_counter == 2:
        qtile.cmd_spawn("alacritty -e nightmode2.sh")
    elif nightmode_counter == 3:
        qtile.cmd_spawn("alacritty -e nightmode3.sh")
    elif nightmode_counter == 4:
        qtile.cmd_spawn("alacritty -e nightmode4.sh")
    else:
        qtile.cmd_spawn("alacritty -e daymode.sh")
    nightmode_counter += 1
    if nightmode_counter >= 6:
        nightmode_counter = 1


# shutdown now
@lazy.function
def shutdown_now(qtile):
    qtile.cmd_spawn("shutdown now")


### KEYS ###
keys = [
    # Key([mod, "shift"], "b", set_bg_image), ### TEST
    # Most of our keybindings are in sxhkd file - except these
    # SUPER + FUNCTION KEYS
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    Key([mod], "q", lazy.window.kill()),
    Key([mod], "b", lazy.hide_show_bar()),  # add and remove top-bar
    Key([mod], "r", lazy.reload_config()),
    # QTILE LAYOUT KEYS
    Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "space", lazy.next_layout()),
    # CHANGE FOCUS
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),
    # Key([mod], "Left", lazy.layout.left()),
    # Key([mod], "Right", lazy.layout.right()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    # RESIZE UP, DOWN, LEFT, RIGHT
    Key(
        [mod, "control"],
        "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
    ),
    Key(
        [mod, "control"],
        "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
    ),
    Key(
        [mod, "control"],
        "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
    ),
    Key(
        [mod, "control"],
        "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
    ),
    Key(
        [mod, "control"],
        "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
    ),
    Key(
        [mod, "control"],
        "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
    ),
    Key(
        [mod, "control"],
        "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
    ),
    Key(
        [mod, "control"],
        "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
    ),
    # FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([mod, "shift"], "f", lazy.layout.flip()),
    # FLIP LAYOUT FOR BSP
    Key([mod, "mod1"], "k", lazy.layout.flip_up()),
    Key([mod, "mod1"], "j", lazy.layout.flip_down()),
    Key([mod, "mod1"], "l", lazy.layout.flip_right()),
    Key([mod, "mod1"], "h", lazy.layout.flip_left()),
    # MOVE WINDOWS UP OR DOWN BSP LAYOUT
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
    # MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Left", lazy.layout.swap_left()),
    Key([mod, "shift"], "Right", lazy.layout.swap_right()),
    # TOGGLE FLOATING LAYOUT
    Key([mod, "shift"], "space", lazy.window.toggle_floating()),
    # START APPLICATIONS
    # Key([mod, "shift"], "p", open_kitty),
    Key([mod, "shift"], "t", lazy.spawn("kitty")),
    Key([mod, "shift"], "Return", lazy.spawn("kitty")),
    Key([mod], "t", lazy.spawn("alacritty")),
    Key([mod], "Return", lazy.spawn("alacritty")),
    Key([mod], "w", lazy.spawn("brave")),
    Key([mod, "shift"], "w", lazy.spawn("firefox")),
    Key([mod, "shift"], "e", lazy.spawn("gnome-text-editor")),
    Key(
        [mod],
        "d",
        lazy.spawn(
            "rofi -no-config -no-lazy-grab -show drun -modi drun -theme ~/.config/qtile/rofi/launcher2.rasi"
        ),
    ),
    # Key([mod], "d", lazy.function(rofi_lancher)),
    # lazy.shutdown()
    Key([mod, "shift"], "delete", lazy.shutdown()),
]  # End of keys


def window_to_previous_screen(qtile, switch_group=False, switch_screen=False):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group, switch_group=switch_group)
        if switch_screen is True:
            qtile.cmd_to_screen(i - 1)


def window_to_next_screen(qtile, switch_group=False, switch_screen=False):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group, switch_group=switch_group)
        if switch_screen is True:
            qtile.cmd_to_screen(i + 1)


keys.extend(
    [
        # MOVE WINDOW TO NEXT SCREEN
        Key(
            [mod, "shift"],
            "Right",
            lazy.function(window_to_next_screen, switch_screen=True),
        ),
        Key(
            [mod, "shift"],
            "Left",
            lazy.function(window_to_previous_screen, switch_screen=True),
        ),
    ]
)

groups = []

# FOR QWERTY KEYBOARDS
group_names = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "0",
]

# FOR AZERTY KEYBOARDS
# group_names = ["ampersand", "eacute", "quotedbl", "apostrophe", "parenleft", "section", "egrave", "exclam", "ccedilla", "agrave",]

group_labels = [
    "1 ",
    "2 ",
    "3 ",
    "4 ",
    "5 ",
    "6 ",
    "7 ",
    "8 ",
    "9 ",
    "0",
]
# group_labels = ["", "", "", "", "", "", "", "", "", "",]
# group_labels = ["Web", "Edit/chat", "Image", "Gimp", "Meld", "Video", "Vb", "Files", "Mail", "Music",]

group_layouts = [
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
]
# group_layouts = ["monadtall", "matrix", "monadtall", "bsp", "monadtall", "matrix", "monadtall", "bsp", "monadtall", "monadtall",]

#
for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        )
    )

for i in groups:
    keys.extend(
        [
            # CHANGE WORKSPACES
            Key([mod], i.name, lazy.group[i.name].toscreen()),
            Key([mod], "Tab", lazy.screen.toggle_group()),
            # Key([mod, "shift" ], "Tab", lazy.screen.prev_group()),
            Key(["mod1"], "Tab", lazy.screen.next_group()),
            Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),
            Key([mod], "Right", lazy.screen.next_group()),
            Key([mod], "Left", lazy.screen.prev_group()),
            # MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
            Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
            # MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
            # Key(
            #     [mod, "shift"],
            #     i.name,
            #     lazy.window.togroup(i.name),
            #     lazy.group[i.name].toscreen(),
            # ),
        ]
    )


# # Add ScratchPad for some applications
# groups.append(
#     [
#         ScratchPad(
#             "scratchpad",
#             [
#                 # define a drop down terminal.
#                 # it is placed in the upper third of screen by default.
#                 DropDown("term", "urxvt", opacity=0.8),
#                 # define another terminal exclusively for ``qtile shell` at different position
#                 DropDown(
#                     "qtile shell",
#                     "urxvt -hold -e 'qtile shell'",
#                     x=0.05,
#                     y=0.4,
#                     width=0.9,
#                     height=0.6,
#                     opacity=0.9,
#                     on_focus_lost_hide=False,
#                 ),
#                 # define an ather DropDown:
#                 DropDown("nnn", "kitty nnn -d -C", opacity=0.8),
#             ],
#         ),
#         Group("a"),
#     ]
# )
#
#
# keys.extend(
#     [
#         # toggle visibiliy of above defined DropDown named "term"
#         Key(["mod"], "F1", lazy.group["scratchpad"].dropdown_toggle("term")),
#         Key(["mod"], "F2", lazy.group["scratchpad"].dropdown_toggle("qtile shell")),
#     ]
# )


def init_layout_theme():
    return {
        "margin": 2,
        "border_width": 2,
        "border_focus": "#5e81ac",
        "border_normal": "#4c566a",
    }


layout_theme = init_layout_theme()


layouts = [
    # layout.MonadTall(margin=8, border_width=2, border_focus="#5e81ac", border_normal="#4c566a"),
    # layout.MonadWide(margin=8, border_width=2, border_focus="#5e81ac", border_normal="#4c566a"),
    # layout.MonadTall(**layout_theme),
    MonadTall(**layout_theme),
    MonadWide(**layout_theme),
    Matrix(**layout_theme),
    Bsp(**layout_theme),
    Floating(**layout_theme),
    RatioTile(**layout_theme),
    Max(**layout_theme),
]


# COLORS FOR THE BAR
# Theme name : ArcoLinux Default
def init_colors():
    return [
        ["#2F343F", "#2F343F"],  # color 0
        ["#2F343F", "#2F343F"],  # color 1
        ["#c0c5ce", "#c0c5ce"],  # color 2
        ["#fba922", "#fba922"],  # color 3
        ["#3384d0", "#3384d0"],  # color 4
        ["#f3f4f5", "#f3f4f5"],  # color 5
        ["#cd1f3f", "#cd1f3f"],  # color 6
        ["#62FF00", "#62FF00"],  # color 7
        ["#6790eb", "#6790eb"],  # color 8
        ["#f9a9a9", "#f9a9a9"],
    ]  # color 9


def init_colors2():
    return [
        ["#fcbc68", "#fcbc68"],  # color 0 light-orange
        ["#2F343F", "#2F343F"],  # color 1 light-gray bg
        ["#c0c5ce", "#c0c5ce"],  # color 2 dark-white
        ["#abfc74", "#abfc74"],  # color 3 light-green
        ["#3384d0", "#3384d0"],  # color 4 blue
        ["#f3f4f5", "#f3f4f5"],  # color 5 white
        ["#cd1f3f", "#cd1f3f"],  # color 6 Red
        ["#62FF00", "#62FF00"],  # color 7 GREEN
        ["#fca7de", "#fca7de"],  # color 8 pink
        ["#7b91bd", "#7b91bd"],  # color 9 blue-gray
    ]


colors = init_colors2()


###########################
### WIDGETS FOR THE BAR ###


def init_widgets_defaults():
    return dict(font="Noto Sans", fontsize=22, padding=2, background=colors[1])


widget_defaults = init_widgets_defaults()


def init_widgets_list():
    # prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())
    widgets_list = [
        widget.GroupBox(
            font="FontAwesome Bold",
            fontsize=23,
            margin_y=+3,
            margin_x=0,
            padding_y=6,
            padding_x=5,
            borderwidth=0,
            disable_drag=True,
            active=colors[5],
            inactive=colors[9],
            rounded=False,
            highlight_method="text",
            this_current_screen_border=colors[3],
            foreground=colors[2],
            background=colors[1],
        ),
        widget.Sep(linewidth=2, padding=20, foreground=colors[2], background=colors[1]),
        widget.CurrentLayout(
            font="FontAwesome",
            fontsize=22,
            foreground=colors[0],
            background=colors[1],
        ),
        widget.Sep(linewidth=2, padding=20, foreground=colors[2], background=colors[1]),
        widget.WindowName(
            font="Noto Sans",
            fontsize=24,
            foreground=colors[8],
            background=colors[1],
        ),
        # widget.Net(
        #          font="Noto Sans",
        #          fontsize=12,
        #          interface="enp0s31f6",
        #          foreground=colors[2],
        #          background=colors[1],
        #          padding = 0,
        #          ),
        # widget.Sep(
        #          linewidth = 1,
        #          padding = 10,
        #          foreground = colors[2],
        #          background = colors[1]
        #          ),
        # widget.NetGraph(
        #          font="Noto Sans",
        #          fontsize=12,
        #          bandwidth="down",
        #          interface="auto",
        #          fill_color = colors[8],
        #          foreground=colors[2],
        #          background=colors[1],
        #          graph_color = colors[8],
        #          border_color = colors[2],
        #          padding = 0,
        #          border_width = 1,
        #          line_width = 1,
        #          ),
        # widget.Sep(
        #          linewidth = 1,
        #          padding = 10,
        #          foreground = colors[2],
        #          background = colors[1]
        #          ),
        # # do not activate in Virtualbox - will break qtile
        # widget.ThermalSensor(
        #          foreground = colors[5],
        #          foreground_alert = colors[6],
        #          background = colors[1],
        #          metric = True,
        #          padding = 3,
        #          threshold = 80
        #          ),
        # # battery option 1  ArcoLinux Horizontal icons do not forget to import arcobattery at the top
        # widget.Sep(
        #          linewidth = 1,
        #          padding = 10,
        #          foreground = colors[2],
        #          background = colors[1]
        #          ),
        # arcobattery.BatteryIcon(
        #          padding=0,
        #          scale=0.7,
        #          y_poss=2,
        #          theme_path=home + "/.config/qtile/icons/battery_icons_horiz",
        #          update_interval = 5,
        #          background = colors[1]
        #          ),
        # # battery option 2  from Qtile
        # widget.Sep(
        #          linewidth = 1,
        #          padding = 10,
        #          foreground = colors[2],
        #          background = colors[1]
        #          ),
        # widget.Battery(
        #          font="Noto Sans",
        #          update_interval = 10,
        #          fontsize = 12,
        #          foreground = colors[5],
        #          background = colors[1],
        #          ),
        # widget.TextBox(
        #          font="FontAwesome",
        #          text="  ",
        #          foreground=colors[6],
        #          background=colors[1],
        #          padding = 0,
        #          fontsize=16
        #          ),
        # widget.CPUGraph(
        #    border_color=colors[2],
        #    fill_color=colors[8],
        #    graph_color=colors[8],
        #    background=colors[1],
        #    border_width=1,
        #    line_width=1,
        #    core="all",
        #    type="box",
        # ),
        # widget.Sep(
        #          linewidth = 1,
        #          padding = 10,
        #          foreground = colors[2],
        #          background = colors[1]
        #          ),
        # widget.TextBox(
        #          font="FontAwesome",
        #          text="  ",
        #          foreground=colors[4],
        #          background=colors[1],
        #          padding = 0,
        #          fontsize=16
        #          ),
        # widget.Memory(
        #          font="Noto Sans",
        #          format = '{MemUsed}M/{MemTotal}M',
        #          update_interval = 1,
        #          fontsize = 12,
        #          foreground = colors[5],
        #          background = colors[1],
        #         ),
        # widget.Sep(
        #          linewidth = 1,
        #          padding = 10,
        #          foreground = colors[2],
        #          background = colors[1]
        #          ),
        widget.Clock(
            foreground=colors[3],
            background=colors[1],
            fontsize=22,
            format="  %Y-%m-%d  ",
        ),
        widget.Clock(
            foreground=colors[8],
            background=colors[1],
            fontsize=22,
            format="%a",
        ),
        widget.TextBox(
            font="FontAwesome",
            text="",
            foreground=colors[4],
            background=colors[1],
            padding=10,
            fontsize=22,
            mouse_callbacks={"Button1": shutdown_now},
        ),
        widget.Clock(
            foreground=colors[8],
            background=colors[1],
            fontsize=22,
            format="%U",
        ),
        widget.Clock(
            foreground=colors[3],
            background=colors[1],
            fontsize=22,
            format="  %H:%M  ",
        ),
        widget.QuickExit(
            font="FontAwesome Bold", foreground=colors[4], default_text="󰍃 "
        ),
        widget.CPU(
            scroll_fixed_width=True,
            update_interval=5,
            format="{load_percent}% ",
            foreground=colors[9],
        ),
        widget.ThermalZone(
            crit=70,
            high=40,
            format_crit="{temp}°C !!!",
            fgcolor_crit="ff0000",
            fgcolor_normal=colors[9],
            fgcolor_high="ffff00",
        ),
        widget.TextBox(
            font="FontAwesome",
            text=" 󰖔 ",
            foreground=colors[3],
            background=colors[1],
            padding=10,
            fontsize=22,
            mouse_callbacks={"Button1": nightmode},
        ),
        widget.Wallpaper(
            background=colors[1],
            foreground=colors[8],
            directory="~/bg/",
            font="FontAwesome",
            option="stretch",
            scroll_clear=True,
            scroll_fixed_width=True,
            label="  ",
        ),
        widget.Sep(linewidth=1, padding=20, foreground=colors[2], background=colors[1]),
        widget.Systray(background=colors[1], icon_size=26, padding=20),
    ]
    return widgets_list


widgets_list = init_widgets_list()


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1


def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2


widgets_screen1 = init_widgets_screen1()
widgets_screen2 = init_widgets_screen2()


def init_screens():
    return [
        Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=26, opacity=0.87)),
        Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=26, opacity=0.87)),
    ]


screens = init_screens()


# MOUSE CONFIGURATION
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
]

dgroups_key_binder = None
dgroups_app_rules = []

# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
# BEGIN


#########################################################
################ assgin apps to groups ##################
#########################################################
@hook.subscribe.client_new
def assign_app_group(client):
    d = {}
    #####################################################################################
    ### Use xprop fo find  the value of WM_CLASS(STRING) -> First field is sufficient ###
    #####################################################################################
    #    d[group_names[0]] = ["kitty", "kitty"]
    #    d[group_names[1]] = ["Alacritty", "Alacritty"]
    d[group_names[2]] = [
        "Navigator",
        "Firefox",
        "Vivaldi-stable",
        "Vivaldi-snapshot",
        "Chromium",
        "Google-chrome",
        "navigator",
        "firefox",
        "vivaldi-stable",
        "vivaldi-snapshot",
        "chromium",
        "google-chrome",
    ]
    d[group_names[3]] = [
        "brave",
        "brave-browser",
        "Brave",
        "Brave-browser",
        "Brave-Browser",
    ]
    d[group_names[4]] = [
        "Atom",
        "Subl",
        "Geany",
        "Brackets",
        "Code-oss",
        "Code",
        "TelegramDesktop",
        "Discord",
        "telegram-desktop",
        "atom",
        "subl",
        "geany",
        "brackets",
        "code-oss",
        "code",
        "telegramDesktop",
        "discord",
    ]
    d[group_names[5]] = ["Vlc", "vlc", "Mpv", "mpv"]
    d[group_names[6]] = ["Gimp", "gimp"]
    #    d[group_names[2]] = ["Inkscape", "Nomacs", "Ristretto", "Nitrogen", "Feh",
    #              "inkscape", "nomacs", "ristretto", "nitrogen", "feh", ]
    #    d[group_names[3]] = ["Gimp", "gimp" ]
    #    d[group_names[4]] = ["Meld", "meld", "org.gnome.meld" "org.gnome.Meld" ]
    #    d[group_names[5]] = ["Vlc","vlc", "Mpv", "mpv" ]
    #    d[group_names[6]] = ["VirtualBox Manager", "VirtualBox Machine", "Vmplayer",
    #              "virtualbox manager", "virtualbox machine", "vmplayer", ]
    #    d[group_names[7]] = ["Thunar", "Nemo", "Caja", "Nautilus", "org.gnome.Nautilus", "Pcmanfm", "Pcmanfm-qt",
    #              "thunar", "nemo", "caja", "nautilus", "org.gnome.nautilus", "pcmanfm", "pcmanfm-qt", ]
    #    d[group_names[8]] = ["Evolution", "Geary", "Mail", "Thunderbird",
    #              "evolution", "geary", "mail", "thunderbird" ]
    #    d[group_names[9]] = ["Spotify", "Pragha", "Clementine", "Deadbeef", "Audacious",
    #              "spotify", "pragha", "clementine", "deadbeef", "audacious" ]
    ######################################################################################

    wm_class = client.window.get_wm_class()[0]

    for i in range(len(d)):
        if wm_class in list(d.values())[i]:
            group = list(d.keys())[i]
            client.togroup(group)
            client.group.cmd_toscreen(toggle=False)


# END
# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME

main = None


# hides the top bar when the archlinux-logout widget is opened
@hook.subscribe.client_new
def new_client(window):
    if window.name == "ArchLinux Logout":
        qtile.hide_show_bar()


# shows the top bar when the archlinux-logout widget is closed
@hook.subscribe.client_killed
def logout_killed(window):
    if window.name == "ArchLinux Logout":
        qtile.hide_show_bar()


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    subprocess.call([home + "/.config/qtile/scripts/autostart.sh"])


@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(["xsetroot", "-cursor_name", "left_ptr"])


@hook.subscribe.client_new
def set_floating(window):
    if (
        window.window.get_wm_transient_for()
        or window.window.get_wm_type() in floating_types
    ):
        window.floating = True


floating_types = ["notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
        Match(wm_class="Arcolinux-welcome-app.py"),
        Match(wm_class="Arcolinux-calamares-tool.py"),
        Match(wm_class="confirm"),
        Match(wm_class="dialog"),
        Match(wm_class="download"),
        Match(wm_class="error"),
        Match(wm_class="file_progress"),
        Match(wm_class="notification"),
        Match(wm_class="splash"),
        Match(wm_class="toolbar"),
        Match(wm_class="Arandr"),
        Match(wm_class="feh"),
        Match(wm_class="Galculator"),
        Match(wm_class="archlinux-logout"),
        Match(wm_class="xfce4-terminal"),
        Match(wm_class="gnome-text-editor"),
    ],
    fullscreen_border_width=0,
    border_width=0,
)
auto_fullscreen = True

focus_on_window_activation = "focus"  # focus or smart

wmname = "LG3D"
