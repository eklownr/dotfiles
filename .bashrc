#  eklow bashrc
#  ____________________________
# /                            \
# | just an ordinary .bashrc   |
# | nothing fancy..            /
# \_________________________'\
#                      ()    \\
#                        o    \\  .
#                          o  |\\/|
#                             / " '\
#                             @ o   .
#                            /    ) |
#                           '  _.'  |
#                           '-'/    \
#
#
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# if not running interactively, don't do anything
case $- in
*i*) ;;
*) return ;;
esac

# don't put duplicate lines or lines starting with space in the history.
# see bash(1) for more options
histcontrol=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see histsize and histfilesize in bash(1)
histsize=10000
histfilesize=20000

# check the window size after each command and, if necessary,
# update the values of lines and columns.
shopt -s checkwinsize

# if set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(shell=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
	debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$term" in
xterm-color | *-256color) color_prompt=yes ;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
	if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
		# we have color support; assume it's compliant with ecma-48
		# (iso/iec-6429). (lack of such support is extremely rare, and such
		# a case would tend to support setf rather than setaf.)
		color_prompt=yes
	else
		color_prompt=
	fi
fi

if [ "$color_prompt" = yes ]; then
	ps1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
	ps1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# if this is an xterm set the title to user@host:dir
case "$term" in
xterm* | rxvt*)
	ps1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$ps1"
	;;
*) ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	alias ls='ls --color=auto'
	#alias dir='dir --color=auto'
	#alias vdir='vdir --color=auto'

	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
fi

# colored gcc warnings and errors
#export gcc_colors='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more aliases
alias ll='ls -alfh'
alias la='ls -a'
alias l='ls -cf'
alias lll='exa -gal --color=always --group-directories-first'
alias a='exa -g --color=always --group-directories-first'
alias aa='exa -gl --color=always --group-directories-first'
alias aaa='exa -gal --color=always --group-directories-first'
alias cp='cp -i'
alias mv='mv -i'
# change directory aliases
alias vf='cd'
alias xs='cd'
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

alias act='source bin/activate'
alias deact='deactivate'
alias venv11='source ~/venv3.11/bin/activate'

alias infofetch='py ~/programmering/python/cli/infofetch.py'
alias neo='python3 ~/neo/infofetch.py'

alias icat="kitty +kitten icat"
alias ipkoll='~/./ipkoll.sh'
alias py='python3'
alias py11='/usr/bin/python3.11'
alias v='vim'
alias nv='~/nvim/bin/nvim'
alias transeng_to_swe='py ~/labb/python/translate_google/translateenglish_to_swedish.py'
alias transswe_to_eng='py ~/labb/python/translate_google/translateswedish_to_english.py'

alias bat='batcat'
alias brc='vim ~/.bashrc'
alias so='source ~/.bashrc'

# alias's for archives
alias mktar='tar -cvf'
alias mkbz2='tar -cvjf'
alias mkgz='tar -cvzf'
alias untar='tar -xvf'
alias unbz2='tar -xvjf'
alias ungz='tar -xvzf'

# alias's to show disk space and space used in a folder
alias diskspace="du -s | sort -n -r |more"
alias folders='du -h --max-depth=1'
alias folderssort='find . -maxdepth 1 -type d -print0 | xargs -0 du -sk | sort -rn'
alias tree='tree -cahf --dirsfirst'
alias treed='tree -cafd'
alias mountedinfo='df -ht'
# search command line history
alias h="history | grep "

# search running processes
alias p="ps aux | awk {' print \$1, \$2, \$9, \$11 '} | grep "
alias topcpu="/bin/ps -eo pcpu,pid,user,args | sort -k 1 -r | head -10 | awk {'print  \$1, \$2, \$3, \$4'}"

#######################################################
# special functions
#######################################################
# extracts any archive(s) (if unp isn't installed)
extract() {
	for archive in "$@"; do
		if [ -f "$archive" ]; then
			case $archive in
			*.tar.bz2) tar xvjf $archive ;;
			*.tar.gz) tar xvzf $archive ;;
			*.bz2) bunzip2 $archive ;;
			*.rar) rar x $archive ;;
			*.gz) gunzip $archive ;;
			*.tar) tar xvf $archive ;;
			*.tbz2) tar xvjf $archive ;;
			*.tgz) tar xvzf $archive ;;
			*.zip) unzip $archive ;;
			*.z) uncompress $archive ;;
			*.7z) 7z x $archive ;;
			*) echo "don't know how to extract '$archive'..." ;;
			esac
		else
			echo "'$archive' is not a valid file!"
		fi
	done
}
# searches for text in all files in the current folder

ftext() {
	# -i case-insensitive
	# -i ignore binary files
	# -h causes filename to be printed
	# -r recursive search
	# -n causes line number to be printed
	# optional: -f treat search term as a literal, not a regular expression
	# optional: -l only print filenames and not the matching lines ex. grep -irl "$1" *
	grep -iihrn --color=always "$1" . | less -r
}

# copy file with a progress bar
cpp() {
	set -e
	strace -q -ewrite cp -- "${1}" "${2}" 2>&1 |
		awk '{
	count += $nf
	if (count % 10 == 0) {
		percent = count / total_size * 100
		printf "%3d%% [", percent
		for (i=0;i<=percent;i++)
			printf "="
			printf ">"
			for (i=percent;i<100;i++)
				printf " "
				printf "]\r"
			}
		}
	end { print "" }' total_size="$(stat -c '%s' "${1}")" count=0
}

# create and go to the directory
mkdirg() {
	mkdir -p "$1"
	cd "$1"
}

# goes up a specified number of directories  (i.e. up 4)
up() {
	local d=""
	limit=$1
	for ((i = 1; i <= limit; i++)); do
		d=$d/..
	done
	d=$(echo $d | sed 's/^\///')
	if [ -z "$d" ]; then
		d=..
	fi
	cd $d
}

# automatically do an ls after each cd, z, or zoxide
#cd (){
#	if [ -n "$1" ]; then
#		builtin cd "$@" && ls
#	else
#		builtin cd ~ && ls
#	fi
#}

# returns the last 2 fields of the working directory
pwdtail() {
	pwd | awk -f/ '{nlast = nf -1;print $nlast"/"$nf}'
}

# git
lazygit() {
	if [ $# -eq 0 ]; then
		echo "Error no comment. Use: lazygit \' comment \'"
	else
		git add .
		git commit -m "$1"
		git push
	fi
}

# add an "alert" alias for long running commands.  use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# alias definitions.
# you may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# see /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		. /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		. /etc/bash_completion
	fi
fi

# color for manpages in less makes manpages a little easier to read
export less_termcap_mb=$'\e[01;31m'
export less_termcap_md=$'\e[01;31m'
export less_termcap_me=$'\e[0m'
export less_termcap_se=$'\e[0m'
export less_termcap_so=$'\e[01;44;33m'
export less_termcap_ue=$'\e[0m'
export less_termcap_us=$'\e[01;32m'

# set the default editor
export editor=vim

### random color script ###
#colorscript random

# fix user's command line mistakes
#eval $(thefuck --alias)

#######################################################
# set the ultimate amazing command prompt
#######################################################

#alias hug="hugo server -f --bind=10.0.0.97 --baseurl=http://10.0.0.97"
#alias web='cd /var/www/html'

bind '"\c-f":"zi\n"'

# a faster way to navigate your filesystem
eval "$(zoxide init bash)"

# starship
eval "$(starship init bash)"

#add go to path
# export path=$path:/usr/local/go/bin

# print ubuntu info at start
#neo
fastfetch -l none --color "yellow"
